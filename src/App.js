import "./App.css";
import Navbar from "./components/Navbar";
import SubHeader from "./components/SubHeader";
import TableContent from "./components/TableContent";

function App() {
  return (
    <div className="App">
      <Navbar />
      <SubHeader />
      <TableContent />
    </div>
  );
}

export default App;
