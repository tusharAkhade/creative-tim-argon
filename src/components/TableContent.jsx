import React from "react";
import { Paper, Toolbar } from "@mui/material";
import Table from "./Table";

function TableContent() {
  return (
    <div className="content-position">
      <Toolbar
        sx={{
          position: "relative",
          top: "-80px",
          left: "250px",
          width: "calc(100% - 250px)",
        }}
      >
        <Paper sx={{ width: "100%" }}>
          <Table mode="Light" />
          <Table mode="Dark" />
        </Paper>
      </Toolbar>
    </div>
  );
}

export default TableContent;
