import React from "react";
import DisplayUsers from "./ui/DisplayUsers";
import ProgressBar from "./ui/ProgressBar";
import ProjectNameIcon from "./ui/ProjectNameIcon";
import StatusIndicator from "./ui/StatusIndicator";
import { data } from "./data";

function Table({ mode }) {
  return (
    <div
      className={mode === "Light" ? "light-table" : "dark-table"}
      style={{ overflowX: "scroll" }}
    >
      <table className="table table-borderless caption-top">
        <caption>
          <div
            className={
              mode === "Light" ? "light-table-caption" : "dark-table-caption"
            }
            style={{
              padding: "20px 24px",
              fontSize: "17px",
              fontWeight: "600",
            }}
          >
            {mode} table
          </div>
        </caption>
        <thead className="">
          <tr
            className={
              mode === "Light" ? "light-table-head" : "dark-table-head"
            }
            style={{
              fontSize: "0.8em",
              fontWeight: "300",
            }}
          >
            <th>
              <div className="table-head-style column-project-width">
                PROJECT
              </div>
            </th>
            <th>
              <div className="table-head-style column-budget-width">BUDGET</div>
            </th>
            <th>
              <div className="table-head-style column-status-width">STATUS</div>
            </th>
            <th>
              <div className="table-head-style column-users-width">USERS</div>
            </th>
            <th>
              <div className="table-head-style column-completion-width">
                COMPLETION
              </div>
            </th>
          </tr>
        </thead>
        <tbody>
          {data.map((data) => {
            if (data.completion <= 60) {
              data.color = "#fb6340";
            } else if (data.completion <= 75) {
              data.color = "#f5365c";
            } else if (data.completion < 100) {
              data.color = "#11cdef";
            } else if (data.completion === 100) {
              data.color = "#2dce89";
            }
            return (
              <tr key={data.project}>
                <td className="table-data-border-bottom">
                  <div className="table-data-style column-project-width">
                    <div className="project-icon">
                      <ProjectNameIcon icon={data.icon} />
                    </div>
                    <div
                      style={{
                        fontSize: "14px",
                        fontWeight: "600",
                      }}
                      className={
                        mode === "Light"
                          ? "light-table-data"
                          : "dark-table-data"
                      }
                    >
                      {data.project}
                    </div>
                  </div>
                </td>
                <td className="table-data-border-bottom">
                  <div
                    style={{
                      fontSize: "13px",
                      fontWeight: "400",
                    }}
                    className={
                      mode === "Light"
                        ? "table-data-style column-budget-width light-table-data"
                        : "table-data-style column-budget-width dark-table-data"
                    }
                  >
                    ${data.budget} USD
                  </div>
                </td>
                <td className="table-data-border-bottom">
                  <div
                    style={{
                      fontSize: "13px",
                      fontWeight: "400",
                    }}
                    className={
                      mode === "Light"
                        ? "table-data-style column-status-width light-table-data"
                        : "table-data-style column-status-width dark-table-data"
                    }
                  >
                    <StatusIndicator color={data.color} /> {data.status}
                  </div>
                </td>
                <td className="table-data-border-bottom">
                  <div className="table-data-style column-users-width">
                    {data.users.map((user, index) => (
                      <DisplayUsers key={user.name} user={user} index={index} />
                    ))}
                  </div>
                </td>
                <td
                  className="table-data-border-bottom"
                  style={{ display: "flex", alignItems: "center" }}
                >
                  <div
                    className={
                      mode === "Light"
                        ? "table-data-style column-status-width light-table-data"
                        : "table-data-style column-status-width dark-table-data"
                    }
                  >
                    <span
                      style={{
                        fontSize: "13px",
                        fontWeight: "400",
                        width: "25px",
                        marginRight: "8px",
                      }}
                    >
                      {data.completion}%
                    </span>
                    <div style={{ marginLeft: "10px" }}>
                      <ProgressBar
                        color={data.color}
                        progressWidth={data.completion}
                      />
                    </div>
                  </div>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

Table.defaultProps = {
  mode: "Light",
};

export default Table;
