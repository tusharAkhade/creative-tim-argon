import React from "react";

function StatusIndicator({ color }) {
  return (
    <div
      style={{
        width: "5px",
        height: "5px",
        borderRadius: "5px",
        backgroundColor: `${color}`,
        marginRight: "10px",
      }}
    ></div>
  );
}

export default StatusIndicator;
