import React from "react";

function ProjectNameIcon({ icon }) {
  return (
    <div>
      <img
        style={{ width: "48px", height: "48px", borderRadius: "48px" }}
        src={icon}
        alt="image"
      />
    </div>
  );
}

export default ProjectNameIcon;
