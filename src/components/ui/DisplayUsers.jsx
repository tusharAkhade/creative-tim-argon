import React from "react";
import Tippy from "@tippyjs/react";
import "tippy.js/dist/tippy.css";

function DisplayUsers({ user, index }) {
  return (
    <div>
      <div className="display-user">
        <Tippy content={user.name}>
          <img
            src={user.imgUrl}
            alt="img1"
            style={{
              width: "36px",
              height: "36px",
              borderRadius: "36px",
              position: "relative",
              right: `${index * 10}px`,
              border: "2px solid #FFF",
            }}
          />
        </Tippy>
      </div>
    </div>
  );
}

export default DisplayUsers;
