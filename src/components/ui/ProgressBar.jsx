import React from "react";

function ProgressBar({ color, progressWidth }) {
  return (
    <div style={{ width: "120px", height: "3px", backgroundColor: "#e9ecef" }}>
      <div
        style={{
          width: `${progressWidth}%`,
          height: "3px",
          backgroundColor: `${color}`,
        }}
      ></div>
    </div>
  );
}

export default ProgressBar;
