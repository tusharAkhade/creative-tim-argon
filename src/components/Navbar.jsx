import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import Divider from "@mui/material/Divider";
import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Logo from "./Logo";
import SearchIcon from "@mui/icons-material/Search";
import InputBase from "@mui/material/InputBase";
import Paper from "@mui/material/Paper";
import { HiBell } from "react-icons/hi";
import { FaObjectUngroup } from "react-icons/fa";
import { RiTv2Line, RiKey2Fill, RiSpaceShipFill } from "react-icons/ri";
import { IoMdPlanet, IoMdPin, IoIosSend } from "react-icons/io";
import { IoPersonCircle } from "react-icons/io5";
import { MdOutlineFormatListBulleted } from "react-icons/md";
import {
  BsFillPersonFill,
  BsFillPaletteFill,
  BsPieChartFill,
} from "react-icons/bs";
import { CgComponents } from "react-icons/cg";

const drawerWidth = 250;

function Navbar(props) {
  const { window } = props;
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const sideBarItems = [
    {
      text: "Dashboard",
      icon: <RiTv2Line color="#5e72e4" size="18" />,
      path: "/dashboard",
    },
    {
      text: "Icons",
      icon: <IoMdPlanet color="#fb6340" size="18" />,
      path: "/",
    },
    {
      text: "Google",
      icon: <IoMdPin color="#5e72e4" size="18" />,
      path: "/",
    },
    {
      text: "Profile",
      icon: <BsFillPersonFill color="#ffd600" size="18" />,
      path: "/",
    },
    {
      text: "Tables",
      icon: <MdOutlineFormatListBulleted color="#172b4d" size="18" />,
      path: "/",
    },
    {
      text: "Login",
      icon: <RiKey2Fill color="#11cdef" size="18" />,
      path: "/",
    },
    {
      text: "Register",
      icon: <IoPersonCircle color="#f3a4b5" size="18" />,
      path: "/",
    },
    {
      text: "Upgrade",
      icon: <IoIosSend color="#212529" size="18" />,
      path: "/",
    },
  ];

  const sideBarIconTwo = [
    {
      text: "Getting Started",
      icon: <RiSpaceShipFill size="18" />,
      path: "/",
    },
    {
      text: "Foundation",
      icon: <BsFillPaletteFill size="18" />,
      path: "/",
    },
    {
      text: "Components",
      icon: <CgComponents size="18" />,
      path: "/",
    },
    {
      text: "Plugins",
      icon: <BsPieChartFill size="18" />,
      path: "/",
    },
  ];

  const drawer = (
    <div>
      <Logo />
      <List>
        {sideBarItems.map((item) => (
          <ListItem button key={item.text}>
            <ListItemIcon>{item.icon}</ListItemIcon>
            <ListItemText primary={item.text} className="list-item-text" />
          </ListItem>
        ))}
      </List>
      <Divider />
      <List>
        <Typography
          variant="subtitle2"
          className="list-item-text"
          sx={{ paddingLeft: "17px", fontWeight: 600 }}
        >
          DOCUMENTATION
        </Typography>
        {sideBarIconTwo.map((item, index) => (
          <ListItem button key={item.text}>
            <ListItemIcon> {item.icon} </ListItemIcon>
            <ListItemText primary={item.text} className="list-item-text" />
          </ListItem>
        ))}
      </List>
    </div>
  );

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <AppBar
        position="fixed"
        sx={{
          width: { sm: `calc(100% - ${drawerWidth}px)` },
          ml: { sm: `${drawerWidth}px` },
          boxShadow: "unset",
          // borderColor: "!important",
          borderBottom: "1px solid rgb(160,160,255)",
        }}
      >
        <Toolbar
          style={{
            height: "78px",
            backgroundColor: "#5e74e4",
          }}
        >
          <div className="header-container">
            <div>
              <Paper sx={{ background: "#FFF", borderRadius: "50px" }}>
                <IconButton sx={{ p: "10px" }} aria-label="search">
                  <SearchIcon />
                </IconButton>
                <InputBase
                  className="seachbar-container"
                  sx={{ ml: 1, flex: 1 }}
                  placeholder="Search"
                />
              </Paper>
            </div>
            <div style={{ display: "flex", alignItems: "center" }}>
              <div className="header-icon">
                <HiBell size="20px" />
              </div>
              <div className="header-icon">
                <FaObjectUngroup size="20px" />
              </div>
              <div
                className="header-icon"
                style={{ display: "flex", alignItems: "center" }}
              >
                <div className="profile-image"></div>
                <span
                  className="profile-image-text"
                  style={{ marginLeft: "10px" }}
                >
                  John Snow
                </span>
              </div>
            </div>
          </div>
        </Toolbar>
      </AppBar>
      <Box
        component="nav"
        sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
        aria-label="mailbox folders"
      >
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: "block", sm: "none" },
            "& .MuiDrawer-paper": {
              boxSizing: "border-box",
              width: drawerWidth,
            },
          }}
        >
          {drawer}
        </Drawer>
        <Drawer
          variant="permanent"
          sx={{
            display: { xs: "none", sm: "block" },
            "& .MuiDrawer-paper": {
              boxSizing: "border-box",
              width: drawerWidth,
            },
          }}
          open
        >
          {drawer}
        </Drawer>
      </Box>
    </Box>
  );
}

export default Navbar;
