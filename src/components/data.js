export const data = [
  {
    icon: "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/bootstrap.jpg",
    project: "Argon Design System",
    budget: "2500",
    status: "pending",
    users: [
      {
        name: "Ryan Tompson",
        imgUrl:
          "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-1.jpg",
      },
      {
        name: "Romina Hadid",
        imgUrl:
          "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-2.jpg",
      },
      {
        name: "Alexander Smith",
        imgUrl:
          "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-3.jpg",
      },
      {
        name: "Jessica Doe",
        imgUrl:
          "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-4.jpg",
      },
    ],
    completion: 60,
    color: "",
  },
  {
    icon: "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/angular.jpg",
    project: "Angular Now UI Kit PRO",
    budget: "1800",
    status: "completed",
    users: [
      {
        name: "Ryan Tompson",
        imgUrl:
          "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-1.jpg",
      },
      {
        name: "Romina Hadid",
        imgUrl:
          "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-2.jpg",
      },
      {
        name: "Alexander Smith",
        imgUrl:
          "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-3.jpg",
      },
      {
        name: "Jessica Doe",
        imgUrl:
          "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-4.jpg",
      },
    ],
    completion: 100,
    color: "",
  },
  {
    icon: "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/sketch.jpg",
    project: "Black Dashboard",
    budget: "3150",
    status: "delayed",
    users: [
      {
        name: "Ryan Tompson",
        imgUrl:
          "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-1.jpg",
      },
      {
        name: "Romina Hadid",
        imgUrl:
          "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-2.jpg",
      },
      {
        name: "Alexander Smith",
        imgUrl:
          "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-3.jpg",
      },
      {
        name: "Jessica Doe",
        imgUrl:
          "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-4.jpg",
      },
    ],
    completion: 72,
    color: "",
  },
  {
    icon: "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/react.jpg",
    project: "React Material Dashboard",
    budget: "4400",
    status: "on schedule",
    users: [
      {
        name: "Ryan Tompson",
        imgUrl:
          "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-1.jpg",
      },
      {
        name: "Romina Hadid",
        imgUrl:
          "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-2.jpg",
      },
      {
        name: "Alexander Smith",
        imgUrl:
          "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-3.jpg",
      },
      {
        name: "Jessica Doe",
        imgUrl:
          "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-4.jpg",
      },
    ],
    completion: 90,
    color: "",
  },
  {
    icon: "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/vue.jpg",
    project: "Vue Paper UI Kit PRO",
    budget: "2200",
    status: "completed",
    users: [
      {
        name: "Ryan Tompson",
        imgUrl:
          "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-1.jpg",
      },
      {
        name: "Romina Hadid",
        imgUrl:
          "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-2.jpg",
      },
      {
        name: "Alexander Smith",
        imgUrl:
          "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-3.jpg",
      },
      {
        name: "Jessica Doe",
        imgUrl:
          "https://demos.creative-tim.com/argon-dashboard/assets/img/theme/team-4.jpg",
      },
    ],
    completion: 100,
    color: "",
  },
];
